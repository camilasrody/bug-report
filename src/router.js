import { createRouter, createWebHistory } from 'vue-router'
import Index from './Index.vue'
import ReportForm from './components/ReportForm.vue'
import NotFound from './NotFound.vue'

/** @type {import('vue-router').RouterOptions['routes']} */
const routes = [
  {
    path: '/',
    meta: {title: 'Index'},
    component: Index
  },
  {
    path: '/report/:id',
    meta: {title: 'Report'},
    // component: Index,
    components: {
      default: Index,
      topBar: ReportForm
    },
    children: [
      {
        // ReportForm will be rendered inside Index's <router-view>
        path: '/report/:id/edit',
        component: ReportForm
      },
    ]
  },
  {
    path: '/report/create',
    components: {
      default: Index,
      topBar: ReportForm
    },
    meta: {
      title: 'Criar ReportForm'
    },
  },

  { path: '/:path(.*)', component: NotFound },
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

export default router
