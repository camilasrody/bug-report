const faunadb = require('faunadb')

const q = faunadb.query
const client = new faunadb.Client({
  secret: process.env.FAUNADB_SECRET
})

exports.handler = async (event, context, callback) => {
  const data = JSON.parse(event.body)
  console.log("Function `bug-create` invoked", data)
  try {
    const bugItem = {
      data: data
    }
    const response = await client.query(q.Create(q.Ref("classes/bugs"), bugItem))

    return callback(null, {
      statusCode: 200,
      body: JSON.stringify(response)
    })
  } catch (error) {
	console.log("error", error)
    return callback(null, {
      statusCode: 400,
      body: JSON.stringify(error)
    })
  }
}
