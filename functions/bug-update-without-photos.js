const faunadb = require('faunadb')

const q = faunadb.query
const client = new faunadb.Client({
  secret: process.env.FAUNADB_SECRET
})

exports.handler = async (event, context, callback) => {
  const data = JSON.parse(event.body)
  const id = event.path.match(/([^\/]*)\/*$/)[0]
  console.log(`Function 'bug-update' invoked. update id: ${id}`)
  try {
    const response = await client.query(q.Update(q.Ref(`classes/bugs/${id}`), {data}))
    console.log("success", response)
    return callback(null, {
      statusCode: 200,
      body: JSON.stringify(response)
    })
  } catch (error) {
    console.log("error", error)
    return callback(null, {
      statusCode: 400,
      body: JSON.stringify(error)
    })
  }
}

