import vue from '@vitejs/plugin-vue'
import { defineConfig } from "vite"
import { resolve } from 'path'
import EnvironmentPlugin from 'vite-plugin-environment'

export default defineConfig({
  plugins: [
    vue(),
    EnvironmentPlugin({
      NODE_ENV: 'local',
      URL: '',
      NODE_VERSION: '',
      REPOSITORY_URL: '',
      COMMIT_REF: '',
      BRANCH: '',
      NETLIFY_IMAGES_CDN_DOMAIN: '',
      CONTEXT: '',
    }, { defineOn: 'import.meta.env' }),
  ],
  resolve: {
    alias: {
      '@': resolve(__dirname, 'src'),
    },
  },
  server: {
    open: true,
  },
})
