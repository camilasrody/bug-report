import Busboy from 'busboy'
import FormData from 'form-data'
import axios from 'axios'
const faunadb = require('faunadb')

const q = faunadb.query
const client = new faunadb.Client({
  secret: process.env.FAUNADB_SECRET
})
const headers = {
	'Access-Control-Allow-Origin': '*',
	// 'Access-Control-Allow-Origin': ( process.env.NODE_ENV === 'production') ? process.env.DEPLOY_PRIME_URL : 'http://localhost:9000/',
	'Access-Control-Allow-Methods': 'POST',
	'Access-Control-Allow-Headers': 'Content-Type'
}
function parseMultipartForm(event) {

  return new Promise((resolve) => {
    const fields = {}

    const busboy = Busboy({
      headers: event.headers,
    })

    busboy.on('file', (fieldname, filestream, filename, _, mimeType) => {
      filestream.on('data', (data) => {
        fields[fieldname] = {
          content: data,
          filename,
          type: mimeType,
        }
      })
    })

    busboy.on('field', (fieldName, value) => {
      fields[fieldName] = value
    })

    busboy.on('finish', () => {
      resolve(fields)
    })

    busboy.write(Buffer.from(event.body, 'base64').toString('utf8'))
  })

}

exports.handler = async function (event, context) {
  try {
    console.log("Function `bug-create` invoked")
    const fields = await parseMultipartForm(event)

    const form = new FormData()

    form.append('file', fields.photos)
    form.append('upload_preset', process.env.CLOUDINARY_UPLOAD_PRESET)

    const response = await axios({
      method: 'post',
      url: `https://api.cloudinary.com/v1_1/${process.env.CLOUDINARY_CLOUD_NAME}/auto/upload`,
      data: form,
      headers: {
        'Content-Type': 'multipart/form-data; boundary=' + form._boundary,
      },
    })

    // overwrite file data
    fields.photos = response.data.secure_url
    const bugItem = {
      data: fields
    }
    const dbResponse = await client.query(q.Create(q.Ref("classes/bugs"), bugItem))

    return {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        ...dbResponse,
        photo: response.data
      })
    }
  } catch (err) {
    console.log(err)
    return {
      statusCode: err.statusCode || 500,
      headers,
      body: JSON.stringify({
        error: err.message
      })
    }
  }
}
