const faunadb = require('faunadb')

// import getId from './utils/getId'

const q = faunadb.query
const client = new faunadb.Client({
  secret: process.env.FAUNADB_SECRET
})
const headers = {
	'Access-Control-Allow-Origin': '*',
	// 'Access-Control-Allow-Origin': ( process.env.NODE_ENV === 'production') ? process.env.DEPLOY_PRIME_URL : 'http://localhost:9000/',
	'Access-Control-Allow-Methods': 'GET',
	'Access-Control-Allow-Headers': 'Content-Type'
}

exports.handler = async (event, context) => {
  const id = event.path.match(/([^\/]*)\/*$/)[0]
  console.log(`Function 'bug-read' invoked. Read id: ${id}`)
  try {
    const response = await client.query(q.Get(q.Ref(`classes/bugs/${id}`)))
    console.log("success", response)
    return {
      statusCode: 200,
      headers,
      body: JSON.stringify(response)
    }
  } catch (err) {
    console.log(err)
    return {
      statusCode: err.statusCode || 500,
      headers,
      body: JSON.stringify({
        error: err.message
      })
    }
  }
}
