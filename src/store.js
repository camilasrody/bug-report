import { reactive } from 'vue'
import api from '@/utils/api'

export const store = reactive({
      title: "",
      description: "",
      priority: "",
      type: "",
      solution: "",
      photos: [],

      successModal: false,
      currentFormModal: false,
      currentDeleteModal: null,

      showDiscartModal: false,
      showConfirmModal: false,
      showNoChanges: false,

      formWasChanged: false,
      currentDefaultFormValues: {
        title: "",
        description: "",
        priority: "",
        type: "",
        solution: "",
        photos: [],
      },

      searchQuery: '',
      searchPriority: ['low', 'min', 'high'],
      items: [],
      ...api,
      setForm(data) {
        this.id = data.id
        this.title = data.title
        this.description = data.description
        this.priority = data.priority
        this.type = data.type
        this.solution = data.solution
        this.photos = data.photos
      },
      setItems(items) {
        this.items = items
      }
    })
