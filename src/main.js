import { createApp } from 'vue'
import '@/assets/css/tailwind.css'
import App from './App.vue'
import router from './router.js'
import 'flowbite'

const app = createApp(App);

app.config.productionTip = false
app
    .use(router)
    .mount('#app')
