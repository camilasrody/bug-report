const faunadb = require('faunadb')
const {parse} = require('querystring')

const q = faunadb.query
const client = new faunadb.Client({
  secret: process.env.FAUNADB_SECRET
})
const headers = {
	'Access-Control-Allow-Origin': '*',
	// 'Access-Control-Allow-Origin': ( process.env.NODE_ENV === 'production') ? process.env.DEPLOY_PRIME_URL : 'http://localhost:9000/',
	'Access-Control-Allow-Methods': 'DELETE',
	'Access-Control-Allow-Headers': 'Content-Type'
}
exports.handler = async (event, context, callback) => {
  const id = event.path.match(/([^\/]*)\/*$/)[0]
  console.log(`Function 'bug-delete' invoked. delete id: ${id}`)
  try {
    const response = await client.query(q.Delete(q.Ref(`classes/bugs/${id}`)))
    console.log("success", response)
    return callback(null, {
      headers,
      statusCode: 200,
      body: JSON.stringify(response)
    })
  }catch (error) {
    console.log("error", error)
    return callback(null, {
      headers,
      statusCode: 400,
      body: JSON.stringify(error)
    })
  }
}
