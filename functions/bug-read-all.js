const faunadb = require('faunadb')

const q = faunadb.query
const client = new faunadb.Client({
  secret: process.env.FAUNADB_SECRET
})
const headers = {
	'Access-Control-Allow-Origin': '*',
	// 'Access-Control-Allow-Origin': ( process.env.NODE_ENV === 'production') ? process.env.DEPLOY_PRIME_URL : 'http://localhost:9000/',
	'Access-Control-Allow-Methods': 'GET',
	'Access-Control-Allow-Headers': 'Content-Type'
}

exports.handler = async (event, context) => {
  if (event.httpMethod !== 'GET') {
		return {
			statusCode: 410,
			headers,
			body: JSON.stringify({
				error: 'Only GET requests allowed.',
			}),
		}
	}
  console.log("Function `bug-read-all` invoked")
  try {
    const response = await client.query(q.Paginate(q.Match(q.Ref("indexes/all_bugs"))))
    const bugRefs = response.data
    console.log("bug refs", bugRefs)
    console.log(`${bugRefs.length} bugs found`)
    // create new query out of bug refs. https://docs.fauna.com/fauna/current/reference/queryapi/index.html
    const getAllBugsDataQuery = bugRefs.map((ref) => {
      return q.Get(ref)
    })
    // then query the refs
    const ret = await client.query(getAllBugsDataQuery)
    return {
      statusCode: 200,
      headers,
      body: JSON.stringify(ret)
    }
  } catch (err) {
    console.log(err)
    return {
      statusCode: err.statusCode || 500,
      headers,
      body: JSON.stringify({
        error: err.message
      })
    }
  }
}
