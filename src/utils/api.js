/* Api methods to call /functions */
const apiBaseUrl = (process.env.NODE_ENV === 'production') ? '/.netlify/functions' : 'http://localhost:9000/.netlify/functions'

const save = async (data) => {
  let form = new FormData()

  for (const [key, value] of Object.entries(data)) {
      form.append(key, value)
  }

  if (data.id) {
    return await update(data.id, form)
  }
  return await create(form)
}

const create = async (form) => {
  return await fetch(`${apiBaseUrl}/bug-create`, {
    method: 'POST',
    body: form,
  })
}

const readAll = async () => {
  const response = await fetch(`${apiBaseUrl}/bug-read-all`)
  return response.json()
}

const read = async (bugId) => {
  const response = await fetch(`${apiBaseUrl}/bug-read/${bugId}`)
  return response.json()
}

const update = async (bugId, form) => {
  return await fetch(`${apiBaseUrl}/bug-update/${bugId}`, {
    method: 'POST',
    body: form,
  })
}

const deleteBug = async (bugId) => {
  return await fetch(`${apiBaseUrl}/bug-delete/${bugId}`, {
    method: 'DELETE'
  })

}


export default {
  save,
  create,
  read,
  readAll,
  update,
  deleteBug,
}
